from transaksi import Transaksi

transaksi = None

def greeting():
    '''
    This function will be run at the first time when program is running
    - You need to input your name (nama) and month (bulan)
    - at the and gonna save your data into variable pembukuan
    '''
    global transaksi
    print("Hai, Selamat datang di aplikasi pembukuan sederhana")
    nama = input("Nama : ")
    bulan = input("Bulan Pembukuan : ")
    transaksi = Transaksi(nama, bulan)
    return transaksi

def show_menu():
    '''
    This function will given show you two options menu
    - Menu number 1: Input Transaksi Baru
    - Menu number 2: Daftar Transaksi
    '''
    print("##### ~ Menu Aplikasi Pembukuan Sederhana ~ #####")
    print("1 Input Transaksi Baru \n2 Daftar Transaksi")
    menu = int(input("Pilih menu yang kamu inginkan (angka) : "))

    if menu == 1:
        menu_1()
    elif menu == 2:
        menu_2()
    else:
        print("Invalid Choice, enter number 1 or number 2")
        show_menu()

def menu_1():
    '''
    This function gonna show when you choose menu number 1
    You need to input :
    - Nama transaksi
    - Jumlah transaksi
    - Tanggal transaksi
    '''
    print("Anda memilih menu 1 untuk input transaksi baru")
    transaksi.add_transaksi()
    show_menu()

def menu_2():
    '''
    This function gonna show when you choose menu number 2
    and gonna show your detail transaction :
    - Nama
    - Bulan
    - Detail transaksi
    at the end given show you two options menu
    - Menu number 1: Back to Main Menu
    - Menu number 2: Exit program
    '''
    data = transaksi.get_pembukuan()
    nama = data["nama"]
    bulan = data["bulan"]

    print("=" * 75)
    print(f"\nLaporan Transaksi Pembukuan si {nama} bulan {bulan}\n")
    print("=" * 75)
    transaksi.detail_transaksi()

    back_to_main_menu_or_exit = int(input("\nTekan 1 untuk kembali ke menu awal, tekan 2 untuk keluar dari program : "))
    if back_to_main_menu_or_exit == 1:
        show_menu()
    else:
        exit
        

greeting()
show_menu()