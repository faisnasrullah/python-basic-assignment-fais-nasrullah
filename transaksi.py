class Transaksi():

    def __init__(self, nama, bulan):
        self.pembukuan = {}
        self.nama = nama
        self.bulan = bulan
        self.total = 0
        self.update_pembukuan()

    def __str__(self) -> str:
        return f'Hai {self.nama} - pembukuan bulan {self.bulan}'

    def __repr__(self) -> str:
        return f'Hai {self.nama} - ini catatan transaksi mu untuk bulan {self.bulan}'

    def get_pembukuan(self):
        '''
        Return all data
        '''
        return self.pembukuan

    def update_pembukuan(self):
        '''
        Function handle update pembukuan when program running at the first time
        '''
        return self.pembukuan.update({"nama": self.nama.title(), "bulan": self.bulan.capitalize(), "transaksi": list()})

    def get_total(self):
        '''
        Return total
        '''
        return self.total

    def update_total(self):
        '''
        Function handle update total after add new transaction
        '''
        temp_total = []
        data = self.pembukuan["transaksi"]

        for result in data:
            jumlah = result["jumlah"]
            temp_total.append(jumlah)
        
        self.total = sum(temp_total)
        return self.total

    def add_transaksi(self):
        '''
        Function handle add new transaction
        '''
        detail_transaksi = {}
        nama_transaksi = str(input("Nama transaksi : "))
        jumlah = int(input("Jumlah transaksi : "))
        tanggal = int(input("Tanggal transaksi (angka) : "))
        detail_transaksi.update({"tanggal": tanggal, "nama": nama_transaksi.title(), "jumlah": jumlah})
        return self.pembukuan["transaksi"].append(detail_transaksi), self.update_total()

    def detail_transaksi(self):
        '''
        Function handle to get details transaction
        '''
        data = self.pembukuan["transaksi"]
        total = self.get_total()

        print("\nTanggal  |  Nama Transaksi     |  Jumlah\n")
        print("-" * 75)
        for result in data:
            gap1 = " " * 5
            gap2 = " " * 27
            print(f"\n{str(result['tanggal']).zfill(2)} {gap1} | {result['nama']}  | {result['jumlah']}\n")
            
        print("-" * 75)
        print(f"\nTotal {gap2} {total}\n")
        print("-" * 75)