# Python Basic Assignment - Fais Nasrullah

Python Basic Assignment - Bootcamp Python Programing Online Logisklik

## Requirements

- Python3

## Setup

The first thing to do is to clone the repository:

```sh
$ git clone https://gitlab.com/faisnasrullah/python-basic-assignment-fais-nasrullah.git
$ cd python-basic-assignment-fais-nasrullah
```

## How to run

```sh
$ python main.py
```
